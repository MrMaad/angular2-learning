import { Component, OnInit } from '@angular/core';

import {Hero} from "./hero";
import {HeroService} from "./hero.service";


@Component({
	selector: 'app-root',
	template: `
		<h1>{{title}}</h1>

		<h2>List of Heroes</h2>
		<ul class="heroes">
			<li *ngFor="let hero of heroes" 
			[class.selected]="hero === selectedHero"
			(click)="onSelect(hero)">
				<span class="badge">{{hero.id}}</span> {{hero.name}}
			</li>
		</ul>		

		<my-hero-detail [iksde]="selectedHero"></my-hero-detail>
	`,
	styleUrls: ["./app.component.css"],
})

export class AppComponent implements OnInit {
	constructor(private heroService: HeroService) {}

	title = "Tour of Heroes";
	heroes: Hero[];
	selectedHero: Hero;

	ngOnInit(): void {
		this.getHeroes();
	}

	getHeroes(): void {
		this.heroService.getHeroes().then(heroes => this.heroes = heroes);
	}

	onSelect(hero: Hero): void {
		this.selectedHero = hero;
	}
}
