import { Component } from "@angular/core";
import { CourseService } from "./course.service";
import { AutoGrowDirective } from "./auto-grow.directive";


@Component({
	selector: "courses",
	templateUrl:"./courses.component.html",
	providers: [CourseService],
})

export class CoursesComponent {
	title = "Lista wszystkich kursów";
	courses;

	constructor(courseService : CourseService) {
		this.courses = courseService.getCourses();
	}
}