import { Component } from "@angular/core";
import { AuthorService } from "./author.service"

@Component({
	selector: "authors",
	templateUrl: "./authors.component.html",
	providers: [AuthorService],
})

export class AuthorsComponent {
	title = "Lista autorów";
	authors;

	constructor(authorService : AuthorService) {
		this.authors = authorService.getAuthors();
	}
}